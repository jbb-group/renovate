Project allows to run renovate bot in [jBB Bulletin Board group](https://gitlab.com/jbb-group) projects.

For more details please follow [Renovate runner readme](https://gitlab.com/renovate-bot/renovate-runner/-/blob/main/README.md).
